# Zopa Lender Widget Exercise

## Build & development

Once cloned the repository, run `grunt --force` for building and `grunt serve` for preview.

## Most relevant tools used

- AngularJS
- HTML/CSS
- SASS
- Grunt

## Developer Mode

To toggle developer mode view On/Off use keys `ctrl + k` on browser.

