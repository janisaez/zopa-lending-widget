'use strict';

/**
 * @ngdoc directive
 * @name lenderWidgetApp.directive:countTo
 * @description
 * # countTo
 */
angular.module('lenderWidgetApp')
  .directive('countTo', ['$timeout', function ($timeout) {
    return {
      replace: false,
      scope: true,
      link: function (scope, element, attrs) {

        var e = element[0];
        var num, refreshInterval, duration, steps, step, countTo, value, increment, count;

        var calculate = function () {
          refreshInterval = 30;
          step = 0;
          scope.timoutId = null;
          countTo = parseFloat(attrs.countTo) || 0;
          scope.value = parseFloat(attrs.value) || 0;

          duration = (parseFloat(attrs.duration) * 1000) || 0;

          steps = Math.ceil(duration / refreshInterval);
          increment = ((countTo - scope.value) / steps);
          num = scope.value;

        };

        var tick = function () {
          scope.timoutId = $timeout(function () {
            num += increment;
            step++;
            if (step >= steps) {
              $timeout.cancel(scope.timoutId);
              num = countTo;
              e.textContent = countTo.toFixed(1);
            } else {
              e.textContent = num.toFixed(1);
              tick();
            }
          }, refreshInterval);

        };

        var start = function () {
          if (scope.timoutId) {
            $timeout.cancel(scope.timoutId);
          }
          calculate();
          tick();
        };

        attrs.$observe('count', function(val){
          scope.count = val;
        });

        attrs.$observe('countTo', function (val) {
          if (val && scope.count === 'true' && typeof(scope.count)!=='undefined') {
            start();
          } else {
            e.textContent = parseFloat(attrs.countTo).toFixed(1) || 0;
          }
        });

        attrs.$observe('value', function (val) {
          if (val && scope.count === 'true' && typeof(scope.count)!=='undefined') {
            start();
          }
        });

        return true;
      }
    }

  }]);
