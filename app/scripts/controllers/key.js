'use strict';

/**
 * @ngdoc function
 * @name lenderWidgetApp.controller:KeyCtrl
 * @description
 * # KeyCtrl
 * Controller of the lenderWidgetApp
 */
angular.module('lenderWidgetApp')
  .controller('KeyCtrl', ['$scope', function ($scope) {
    $scope.devMode = false;
    $scope.keyBuffer = [];

    $scope.toggleDeveloperMode = function(value){
      if ($scope.keyBuffer.length> 1) {
        if(value.keyCode === 75 && $scope.keyBuffer[$scope.keyBuffer.length-1] === 17) {
          $scope.devMode = !$scope.devMode;
          //console.log('devmode: ' + $scope.devMode);
          $scope.$broadcast('toggleDevMode', {value: $scope.devMode});
        }
      }
      $scope.keyBuffer.push(value.keyCode);
    };
  }]);
