'use strict';

/**
 * @ngdoc function
 * @name lenderWidgetApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the lenderWidgetApp
 */
angular.module('lenderWidgetApp')
  .controller('MainCtrl', ['$scope', function ($scope) {
    var vm = this;

    vm.duples = [{years: '5', annualisedReturn: '3.6'}, {years: '3', annualisedReturn: '3.0'}];
    vm.selected = vm.duples[0];

    vm.startCount = false;
    vm.counter = {'countFrom': vm.duples[1].annualisedReturn, 'countTo': vm.duples[0].annualisedReturn};

    vm.devMode = false;

    $scope.$on('toggleDevMode', function(event, data){
      console.log('got to toggle dev mode to '+ data.value);
      vm.devMode = data.value;
    });

    $scope.$watch(function(){return vm.selected.years;}, function(newVal, oldVal){
      if (oldVal !== newVal){
        vm.startCount = true;
        var tempCountTo = vm.counter.countTo;
        vm.counter.countTo = vm.counter.countFrom;
        vm.counter.countFrom = tempCountTo;
      }
    });


  }]);
